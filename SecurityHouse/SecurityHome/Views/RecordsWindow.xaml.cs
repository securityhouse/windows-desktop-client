﻿using DPUruNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


using SecurityHouse.Models;

namespace SecurityHouse.Views {
    /// <summary>
    /// Interaction logic for RecordsWindow.xaml
    /// </summary>
    public partial class RecordsWindow : Window {
        public RecordsWindow() {
            InitializeComponent();
            getRecords();

        }

        private void getRecords() {
            RestAPI.getRecord(SH.UserAccessToken, (IRestResponse response) => {
                if (response.StatusCode == System.Net.HttpStatusCode.OK && response.Content != "") {
                    JObject json = JObject.Parse(response.Content);
                    var records = JsonConvert.DeserializeObject<List<Record>>(json["data"].ToString());
                    setDataGridRecordsItemsSource(records);
                }
            });
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e) {
            getRecords();
        }

        private void setDataGridRecordsItemsSource(List<Record> records) {
            Dispatcher.InvokeAsync(() => {
                DataGridRecords.ItemsSource = records;
            });
        }
    }
}
