﻿using DPUruNet;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;

namespace SecurityHouse.Views {
    /// <summary>
    /// Lógica de interacción para UpdateProfileWindow.xaml
    /// </summary>
    public partial class UpdateProfileWindow : Window {

        private List<Fmd> fmdsToEnrole;
        public byte[] bytesFingerprintCapture;

        public UpdateProfileWindow() {
            InitializeComponent();
            fmdsToEnrole = new List<Fmd>();
            if(SH.User != null) {
                TextBoxName.Text = SH.User.name.first;
                TextBoxLastName.Text = SH.User.name.last;
                TextBoxIdHouse.Text = SH.User.idHouse;
                if (!string.IsNullOrEmpty(SH.User.fingerprint)) {
                    LabelHuellaCapturada.Content = "SI";
                } else {
                    LabelHuellaCapturada.Content = "NO";
                }
            }
            if (MainWindow.SelectedReader != null) {
                MainWindow.SelectedReader.On_Captured += readerOnCaptured;
            } else {
                MessageBox.Show("Al parecer el lector de huellas no esta inicializado.", "Lector de huellas no inicializado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void readerOnCaptured(CaptureResult result) {
            if (result.ResultCode != Constants.ResultCode.DP_DEVICE_FAILURE) {
                processFingerPrint(result);
            }
        }
        protected void processFingerPrint(CaptureResult captureResult) {
            if (captureResult.Quality == Constants.CaptureQuality.DP_QUALITY_GOOD) {
                Console.WriteLine("Huella capturada con buena calidad");
                var fiv = captureResult.Data.Views[0];
                //Convert.ToBase64String(fiv.Bytes);
                //Convert.FromBase64String("hola");
                var bitmap = Helpers.CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                updateFingerPrintImage(bitmap);
                try {
                    var resultadoConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);               
                    fmdsToEnrole.Add(resultadoConversion.Data);
                    fmdsToEnrole.Add(resultadoConversion.Data);
                    if (fmdsToEnrole.Count == 2) {
                        updateLabelStatus("Huella capturada, Coloque el dedo una vez mas.");
                    }
                    if (fmdsToEnrole.Count == 4) {
                        var fmdsEnrolResult = Enrollment.CreateEnrollmentFmd(Constants.Formats.Fmd.ANSI, fmdsToEnrole);
                        if (fmdsEnrolResult.ResultCode == Constants.ResultCode.DP_SUCCESS) {
                            fmdsToEnrole.Clear();
                            bytesFingerprintCapture = fmdsEnrolResult.Data.Bytes;
                            MessageBox.Show("La plantilla de la huella se ha generado satisfactoriamente", "Plantilla generada", MessageBoxButton.OK, MessageBoxImage.Information);
                            updateLabelStatus("Huella capturada correctamente.");
                        } else if (fmdsEnrolResult.ResultCode == Constants.ResultCode.DP_ENROLLMENT_NOT_READY) {
                            fmdsToEnrole.Clear();
                            bytesFingerprintCapture = null;
                            updateFingerPrintImage(null);
                            MessageBox.Show("No se pudo generar la plantilla correctamente, esto pudo ser debido a que cambiaron de dedo cuando se estaban capturando las huellas.", "Error generando plantilla", MessageBoxButton.OK, MessageBoxImage.Error);
                            updateLabelStatus("Error generando la huella.");
                        } else {
                            Console.WriteLine(fmdsEnrolResult.ResultCode.ToString());
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine("Error:  " + ex.Message);
                }
            } else {
                updateLabelStatus("Huella capturada con mala calidad.");
            }
        }

        public void updateFingerPrintImage(Bitmap bitmap) {
            Dispatcher.InvokeAsync(() => {
                if(bitmap != null) {
                    var bitmapImage = Helpers.BitmapToImageSource(bitmap);
                    ImageFingerPrint.Source = bitmapImage;
                } else {
                    ImageFingerPrint.Source = null;
                }
            });
        }

        public void updateLabelStatus(string status) {
            Dispatcher.InvokeAsync(() => {
                LabelStatus.Content = status;
            });
        }

        private void ButtonUpdateProfile_Click(object sender, RoutedEventArgs e) {
            Console.WriteLine("Click");
            string name = TextBoxName.Text;
            string lastname = TextBoxLastName.Text;
            string idHouse = TextBoxIdHouse.Text;
            if (name == "" || lastname == "" || idHouse == "" || (bytesFingerprintCapture == null && SH.User.bytesFingerprint == null)) {
                MessageBox.Show("Para poder actualizar los datos debe ingresar su nombre, apellido, id de la casa y su huella.", "Datos incompletos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(bytesFingerprintCapture != null) {
                SH.User.bytesFingerprint = bytesFingerprintCapture;
            }
            SH.User.name.first = name;
            SH.User.name.last = lastname;
            SH.User.idHouse = idHouse;
            Console.WriteLine(SH.User.fingerprint);
            RestAPI.updateProfile(Properties.UserSettings.Default.UserAccessToken, SH.User, (IRestResponse response) => {
                Console.WriteLine(response.Content);
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if(MainWindow.SelectedReader != null) MainWindow.SelectedReader.On_Captured -= readerOnCaptured;
        }
    }
}
