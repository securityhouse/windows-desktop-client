﻿using System;
using System.Windows;
using System.Windows.Input;
using RestSharp;
using Newtonsoft.Json;

namespace SecurityHouse.Views {
    /// <summary>
    /// Lógica de interacción para SignUpWindow.xaml
    /// </summary>
    public partial class SignUpWindow : Window {
        public SignUpWindow() {
            InitializeComponent();
            DataContext = this;
        }

        private void buttonSignUp_Click(object sender, RoutedEventArgs e) {
            Console.WriteLine("CLICK");
            string username = textBoxUsername.Text;
            string password = textBoxPassword.Password;
            string confirmPassword = textBoxConfirmPassword.Password;
            if(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(confirmPassword)) {
                MessageBox.Show("Debes llenar todos los campos para poder registrarte.", "Campos incompletos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if(password != confirmPassword) {
                MessageBox.Show("Su contraseña y su confirmacion deben ser las mismas.", "Contraseñas no concuerdan", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
            }
            showProgressModal();
            RestAPI.signUp(username, password, (IRestResponse response) => {
                Console.WriteLine(response.Content);
                hideProgressModal();
                if(response.StatusCode == System.Net.HttpStatusCode.InternalServerError) {
                    MessageBox.Show("Algo malo ha ocurrido en el servidor.", "Error interno del servidor", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (response.StatusCode == System.Net.HttpStatusCode.Conflict) {
                    MessageBox.Show("El usuario que desea registra ya existe, por favor ingrese uno diferente.", "Error en el registro", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (response.StatusCode == System.Net.HttpStatusCode.Created) {
                    MessageBox.Show("Tu cuenta ha sido registrada satisfactoriamente.", "Cuenta registrada", MessageBoxButton.OK, MessageBoxImage.Information);
                    showLoginWindow();
                    return;
                }
            });
        }

        private void LabelLogin_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            showLoginWindow();
        }

        public void showProgressModal() {
            Dispatcher.Invoke(() => {
                ProgressModal.Visibility = Visibility.Visible;
            });
        }

        public void hideProgressModal() {
            Dispatcher.Invoke(() => {
                ProgressModal.Visibility = Visibility.Hidden;
            });
        }

        private void showLoginWindow() {
            Dispatcher.InvokeAsync(() => {
                new LoginWindow().Show();
                Close();
            });
        }
    }
}
