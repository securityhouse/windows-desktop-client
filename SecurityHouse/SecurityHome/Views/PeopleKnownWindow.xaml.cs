﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using DPUruNet;
using System.Drawing;
using SecurityHouse.Models;
using RestSharp;

namespace SecurityHouse.Views {
    /// <summary>
    /// Lógica de interacción para PeopleKnownWindow.xaml
    /// </summary>
    public partial class PeopleKnownWindow : Window {
        private List<Fmd> fmdsToEnrole;
        public byte[] bytesFingerprintCapture;

        public PeopleKnownWindow() {
            InitializeComponent();
            fmdsToEnrole = new List<Fmd>();
            if(SH.User != null) {
                DataGridPeopleKnown.ItemsSource = SH.User.peopleKnown;
            }
            if (MainWindow.SelectedReader != null) {
                MainWindow.SelectedReader.On_Captured += readerOnCaptured;
            } else {
                MessageBox.Show("Al parecer el lector de huellas no esta inicializado.", "Lector de huellas no inicializado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        void readerOnCaptured(CaptureResult result) {
            if (result.ResultCode != Constants.ResultCode.DP_DEVICE_FAILURE) {
                processFingerPrint(result);
            }
        }

        protected void processFingerPrint(CaptureResult captureResult) {
            if (captureResult.Quality == Constants.CaptureQuality.DP_QUALITY_GOOD) {
                Console.WriteLine("Huella capturada con buena calidad");
                var fiv = captureResult.Data.Views[0];
                var bitmap = Helpers.CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                updateFingerPrintImage(bitmap);
                try {
                    var resultadoConversion = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                    fmdsToEnrole.Add(resultadoConversion.Data);
                    fmdsToEnrole.Add(resultadoConversion.Data);
                    if (fmdsToEnrole.Count == 2) {
                        updateLabelStatus("Huella capturada, Coloque el dedo una vez mas.");
                    }
                    if (fmdsToEnrole.Count == 4) {
                        var fmdsEnrolResult = Enrollment.CreateEnrollmentFmd(Constants.Formats.Fmd.ANSI, fmdsToEnrole);
                        if (fmdsEnrolResult.ResultCode == Constants.ResultCode.DP_SUCCESS) {
                            fmdsToEnrole.Clear();
                            bytesFingerprintCapture = fmdsEnrolResult.Data.Bytes;
                            MessageBox.Show("La plantilla de la huella se ha generado satisfactoriamente", "Plantilla generada", MessageBoxButton.OK, MessageBoxImage.Information);
                            updateLabelStatus("Huella capturada correctamente.");
                        } else if (fmdsEnrolResult.ResultCode == Constants.ResultCode.DP_ENROLLMENT_NOT_READY) {
                            fmdsToEnrole.Clear();
                            bytesFingerprintCapture = null;
                            updateFingerPrintImage(null);
                            MessageBox.Show("No se pudo generar la plantilla correctamente, esto pudo ser debido a que cambiaron de dedo cuando se estaban capturando las huellas.", "Error generando plantilla", MessageBoxButton.OK, MessageBoxImage.Error);
                            updateLabelStatus("Error generando la huella.");
                        } else {
                            Console.WriteLine(fmdsEnrolResult.ResultCode.ToString());
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine("Error:  " + ex.Message);
                }
            } else {
                updateLabelStatus("Huella capturada con mala calidad.");
            }
        }

        public void updateFingerPrintImage(Bitmap bitmap) {
            Dispatcher.InvokeAsync(() => {
                if (bitmap != null) {
                    var bitmapImage = Helpers.BitmapToImageSource(bitmap);
                    ImageFingerprint.Source = bitmapImage;
                } else {
                    ImageFingerprint.Source = null;
                }
            });
        }

        public void updateLabelStatus(string status) {
            Dispatcher.InvokeAsync(() => {
                LabelStatusBar.Content = status;
            });
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if (MainWindow.SelectedReader != null) MainWindow.SelectedReader.On_Captured -= readerOnCaptured;
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e) {
            Console.WriteLine("Click");
            string firstName = TextBoxFirstName.Text;
            string lastName = TextBoxLastName.Text;
            string alias = TextBoxAlias.Text;
            if (firstName == "" || lastName == "" || alias == "" || bytesFingerprintCapture == null) {
                MessageBox.Show("Para poder actualizar los datos debe ingresar su nombre, apellido, id de la casa y su huella.", "Datos incompletos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            PersonKnown person = new PersonKnown();
            if (bytesFingerprintCapture != null) {
                person.bytesFingerprint = bytesFingerprintCapture;
            }
            person.name.first = firstName;
            person.name.last = lastName;
            person.alias = alias;
            RestAPI.addPersonKnown(Properties.UserSettings.Default.UserAccessToken, SH.User._id, person, (IRestResponse response) => {
                Console.WriteLine(response.Content);
                Helpers.proccessGetUserContent(response.Content, () => {
                    setDataGridPeopleKnown(SH.User.peopleKnown);
                });
            });
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e) {
            if(DataGridPeopleKnown.SelectedItem != null) {
                PersonKnown pk = (PersonKnown)DataGridPeopleKnown.SelectedItem;
                RestAPI.deletePersonKnown(Properties.UserSettings.Default.UserAccessToken, SH.User._id, pk._id, (IRestResponse response) => {
                    Console.WriteLine(response.Content);

                    RestAPI.getUser(Properties.UserSettings.Default.UserAccessToken, Properties.UserSettings.Default.UserId, (IRestResponse response2) => {
                        Console.WriteLine(response2.Content);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK) {
                            Helpers.proccessGetUserContent(response2.Content, () => {
                                setDataGridPeopleKnown(SH.User.peopleKnown);
                            });
                        }
                    });
                });
            }
        }

        private void setDataGridPeopleKnown(List<PersonKnown> peopleKnow) {
            Dispatcher.InvokeAsync(()=> {
                DataGridPeopleKnown.ItemsSource = peopleKnow;
            });
        }
    }
}
