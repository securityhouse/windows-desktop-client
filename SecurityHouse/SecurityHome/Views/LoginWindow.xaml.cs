﻿using System;
using System.Windows;
using System.Windows.Input;

using SecurityHouse.Models;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SecurityHouse.Views {
    /// <summary>
    /// Lógica de interacción para LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window {
        public LoginWindow() {
            InitializeComponent();
            string accessToken = Properties.UserSettings.Default.UserAccessToken;
            if (!string.IsNullOrEmpty(accessToken)) {
                showMainWindow();
            }
        }

        private void buttonLogin_Click(object sender, RoutedEventArgs e) {
            Console.WriteLine("CLICK");
            string username = textBoxUsername.Text;
            string password = textBoxPassword.Password;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) {
                MessageBox.Show("Debes llenar todos los campos para poder registrarte.", "Campos incompletos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            showProgressModal();
            RestAPI.login(username, password, (IRestResponse response) => {
                Console.WriteLine(response.StatusCode);
                Console.WriteLine(response.Content);
                switch (response.StatusCode) {
                    case System.Net.HttpStatusCode.OK:
                        JObject json = JObject.Parse(response.Content);
                        string token = json["data"]["token"].ToString();
                        SH.User = JsonConvert.DeserializeObject<User>(json["data"]["user"].ToString());
                        Properties.UserSettings.Default.UserAccessToken = token;
                        Properties.UserSettings.Default.UserId = SH.User._id;
                        Properties.UserSettings.Default.Save();
                        hideProgressModal();
                        showMainWindow();
                        break;
                    case System.Net.HttpStatusCode.InternalServerError:
                        MessageBox.Show("Algo malo ha ocurrido en el servidor.", "Error interno del servidor", MessageBoxButton.OK, MessageBoxImage.Error);
                        hideProgressModal();
                        break;
                    case System.Net.HttpStatusCode.Unauthorized:
                        MessageBox.Show("El usuario o contraseña que has ingresado son incorrectos.", "Error en la autenticacion", MessageBoxButton.OK, MessageBoxImage.Error);
                        hideProgressModal();
                        break;
                    default:
                        MessageBox.Show("Servidor caido.", "Servidor caido", MessageBoxButton.OK, MessageBoxImage.Error);
                        hideProgressModal();
                        break;
                }
            });
        }

        private void LabelSignUp_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            var w = new SignUpWindow();
            w.Show();
            Close();
        }

        public void showProgressModal() {
            Dispatcher.Invoke(() => {
                ProgressModal.Visibility = Visibility.Visible;
            });
        }

        public void hideProgressModal() {
            Dispatcher.Invoke(() => {
                ProgressModal.Visibility = Visibility.Hidden;
            });
        }

        private void showMainWindow() {
            Dispatcher.InvokeAsync(() => {
                var w = new MainWindow();
                w.Show();
                Close();
            });
        }
        
    }
}
