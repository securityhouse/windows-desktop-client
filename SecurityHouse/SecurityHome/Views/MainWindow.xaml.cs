﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using DPUruNet;
using System.Drawing;
using RestSharp;
using Newtonsoft.Json;
using SecurityHouse.Models;
using uPLibrary.Networking.M2Mqtt;
using System.Text;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace SecurityHouse.Views {
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public const int THRESH_HOLDER_SCORE = 0x7fffffff * 1 / 100000;
        public const int DELAY_SECONDS = 5;

        public ImmutablePort SelectedPort { set; get; }
        public ImmutablePort ArduinoConnectedPort { set; get; }
                

        private SerialPort arduinoSerialPort;

        public static Reader SelectedReader;
        
        public byte[] BytesHuellaDactilarCapturada;
        //public Fmd PlantillaAVerificar;
        //public bool PlantillaVerificada;

        private bool doProccessFingerprint;
        private MqttClient mqttClient;

        public string TOPIC;

        public MainWindow(){
            InitializeComponent();
            SH.UserAccessToken = Properties.UserSettings.Default.UserAccessToken;
            SH.UserId = Properties.UserSettings.Default.UserId;
            Console.WriteLine(SH.UserAccessToken);
            RestAPI.getUser(SH.UserAccessToken, SH.UserId, (IRestResponse response) => {
                Console.WriteLine(response.Content);
                if (response.StatusCode == System.Net.HttpStatusCode.OK){
                    Helpers.proccessGetUserContent(response.Content, ()=> {
                        mqttClient = new MqttClient("test.mosquitto.org");
                        string clientId = Guid.NewGuid().ToString();
                        mqttClient.MqttMsgPublishReceived += onMqttMsgPublishReceived;
                        mqttClient.Connect(clientId);
                        TOPIC = "/securityhouse/to/" + SH.User._id + "/house/" + SH.User.idHouse;
                        Console.WriteLine("Subscribing to " + TOPIC + " ...");
                        mqttClient.Subscribe(new string[] { TOPIC }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                    });
                }else if (response.StatusCode == 0){
                    MessageBox.Show("Servidor caido.", "Servidor caido", MessageBoxButton.OK, MessageBoxImage.Error);
                    this.IsEnabled = false;
                }
            });
            setupReader();
            ArduinoPortsManager.Instance.OnPortsChanged += onPortsChanged;
            if (ArduinoPortsManager.Instance.Ports.Count > 0) {
                Console.WriteLine("Seting SelectedPort");
                setComboBoxPortsItemSource(ArduinoPortsManager.Instance.Ports);
                setComboBoxPortsSelectedItem(ArduinoPortsManager.Instance.Ports[0]);
            } else {
                setComboBoxPortsIsEnable(false);
                setButtonConnectArduinoIsEnable(false);
            }
            doProccessFingerprint = true;
        }

        #region Events

        #region Widgets events
        private void MenuItemLogout_Click(object sender, RoutedEventArgs e) {
            Properties.UserSettings.Default.UserAccessToken = "";
            Properties.UserSettings.Default.UserId = -1;
            Properties.UserSettings.Default.Save();
            var v = new LoginWindow();
            v.Show();
            Close();
        }

        private void MenuItemClose_Click(object sender, RoutedEventArgs e) {
            Close();
        }

        private void MenuItemProfile_Click(object sender, RoutedEventArgs e) {
            doProccessFingerprint = false;
            var v = new UpdateProfileWindow();
            v.ShowDialog();
            doProccessFingerprint = true;
        }

        private void MenuItemPeopleKnown_Click(object sender, RoutedEventArgs e) {
            doProccessFingerprint = false;
            var v = new PeopleKnownWindow();
            v.ShowDialog();
            doProccessFingerprint = true;
        }

        private void comboBoxPortsSelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e) {
            Console.WriteLine("comboBoxPortsSelectionChanged");
            if (comboBoxPorts.SelectedItem == null) SelectedPort = null;
            else SelectedPort = (ImmutablePort)comboBoxPorts.SelectedItem;
        }

        private void buttonConnectArduino_Click(object sender, RoutedEventArgs e) {
            connectToArduino();
        }

        private void buttonInitializeReader_Click(object sender, RoutedEventArgs e) {
            setupReader();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if (SelectedReader != null) {
                SelectedReader.On_Captured -= readerOnCaptured;
                SelectedReader.CancelCapture();
                SelectedReader.Dispose();
                //FingerPrint.closeReader(SelectedReader);
            }
            if(mqttClient != null) {
                mqttClient.MqttMsgPublishReceived -= onMqttMsgPublishReceived;
                if (mqttClient.IsConnected) {
                    mqttClient.Disconnect();
                }
            }
        }
        #endregion

        #region Internal events
        void onPortsChanged(object sender, EventArgs e) {
            Console.WriteLine("onPortsChanged");
            if (ArduinoPortsManager.Instance.Ports.Count <= 0) {
                setComboBoxPortsItemSource(null);
                setComboBoxPortsIsEnable(false);
                setButtonConnectArduinoIsEnable(false);
                setContentLabelArduinoStatus("Disconected");
                setContentLabelArduinoPort("");
                return;
            }
            setComboBoxPortsItemSource(ArduinoPortsManager.Instance.Ports);
            setComboBoxPortsIsEnable(true);
            setButtonConnectArduinoIsEnable(true);
            ImmutablePort SP = null;
            if (SelectedPort != null) SP = ArduinoPortsManager.Instance.Ports.Find((p) => p.Id == SelectedPort.Id);
            if (SP != null) setComboBoxPortsSelectedItem(SP);
            else setComboBoxPortsSelectedItem(ArduinoPortsManager.Instance.Ports[0]);
        }

        void readerOnCaptured(CaptureResult result) {
            if (result.ResultCode == Constants.ResultCode.DP_DEVICE_FAILURE) {
                setButtonInitializeReaderIsEnable(true);
                SelectedPort = null;
                setContentLabelReaderStatus("Desconectado");
                FingerPrint.closeReader(SelectedReader);
                MessageBox.Show("Al parecer se ha desconectado el lector de huellas del computador, Conectelo de nuevo he intente inicializar de nuevo el lector", "Lector de huellas desconectado", MessageBoxButton.OK, MessageBoxImage.Error);
            } else {
                if(doProccessFingerprint) processFingerPrint(result);
            }
        }

        private void onMqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) {
            Console.WriteLine("Se recivio un mensaje XD");
            var message = Encoding.UTF8.GetString(e.Message);
            Console.WriteLine(message);
            var mqttMessageReceived = JsonConvert.DeserializeObject<MQTTMessageReceived>(message);

            if (mqttMessageReceived.type == "od") {
                var response = new MQTTMessageToSend<MQTTResponseToSendData>();
                response.replyTo = TOPIC;
                response.type = "response";
                var data = new MQTTResponseToSendData();
                data.houseId = SH.User.idHouse;
                if (openDoorAndCloseAfter(DELAY_SECONDS)) {
                    data.message = "success_od";
                } else {
                    data.message = "failed_od";
                }
                response.data = data;
                var stringResponse = JsonConvert.SerializeObject(response);
                Console.WriteLine(stringResponse);
                mqttClient.Publish(mqttMessageReceived.replyTo, Encoding.UTF8.GetBytes(stringResponse), MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, false);
            }
            
        }
        #endregion

        #endregion

        #region Method to manage visual things

        #region ComboBox Ports
        private void setComboBoxPortsItemSource(List<ImmutablePort> ports) {
            Dispatcher.InvokeAsync(() => {
                comboBoxPorts.ItemsSource = ports;
            });
        }

        private void setComboBoxPortsSelectedItem(ImmutablePort port) {
            Dispatcher.InvokeAsync(() => {
                comboBoxPorts.SelectedItem = port;
                comboBoxPorts.IsEnabled = true;
            });
        }

        private void setComboBoxPortsIsEnable(bool isEnabled) {
            Dispatcher.InvokeAsync(() => {
                comboBoxPorts.IsEnabled = isEnabled;
            });
        }
        #endregion

        #region Button Connect Arduino
        private void setButtonConnectArduinoIsEnable(bool isEnabled) {
            Dispatcher.InvokeAsync(() => {
                buttonConnectArduino.IsEnabled = isEnabled;
            });
        }
        #endregion

        private void setContentLabelArduinoPort(string content) {
            Dispatcher.InvokeAsync(() => {
                labelArduinoPort.Content = content;
            });
        }

        private void setContentLabelArduinoStatus(string content) {
            Dispatcher.InvokeAsync(() => {
                labelArduinoStatus.Content = content;
            });
        }

        private void setContentLabelReaderStatus(string content) {
            Dispatcher.InvokeAsync(() => {
                labelFingerprintReaderStatus.Content = content;
            });
        }

        private void setButtonInitializeReaderIsEnable(bool isEnabled) {
            Dispatcher.InvokeAsync(() => {
                buttonInitializeReader.IsEnabled = isEnabled;
            });
        }
        
        private void updateFingerPrintImage(Bitmap bitmap) {
            Dispatcher.InvokeAsync(() => {
                var bitmapImage = Helpers.BitmapToImageSource(bitmap);
                imageFingerPrint.Source = bitmapImage;
            });
        }
        #endregion

        #region Internal methods
        public void connectToArduino() {
            if (SelectedPort == null) {
                MessageBox.Show("No has seleccionado ningun puerto para intentar connectarse a arduino.", "Puerto no seleccionado", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            bool connect = true;
            if (arduinoSerialPort != null) {
                if (arduinoSerialPort.IsOpen) {
                    if (SelectedPort.Id == ArduinoConnectedPort.Id) {
                        MessageBox.Show("Ya hay una conexion establecida con el arduino conectado en este puerto, no es necesario volver a conetar.", "Conexion ya establecida", MessageBoxButton.OK, MessageBoxImage.Information);
                        connect = false;
                    } else {
                        var res = MessageBox.Show("Ya hay una conexion realizada con un arduino conectado en el puerto " + ArduinoConnectedPort.Id + ", ¿Desea desconectarese de este ardunio e intentar conectarse al nuevo puerto?", "Confirmacion", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (res == MessageBoxResult.No) {
                            connect = false;
                        } else {
                            arduinoSerialPort.Close();
                            arduinoSerialPort = null;
                        }
                    }
                }
            }
            if (connect) {
                Console.WriteLine("Connecting...");
                if (openSerialPort(SelectedPort)) {
                    ArduinoConnectedPort = SelectedPort;
                    setContentLabelArduinoPort(SelectedPort.Id);
                    setContentLabelArduinoStatus("Conectado");
                    setButtonConnectArduinoIsEnable(false);
                    MessageBox.Show("La conexion con arduino se ha establecido exitosamente.", "Conexion exitosa", MessageBoxButton.OK, MessageBoxImage.Information);
                } else {
                    ArduinoConnectedPort = null;
                    setContentLabelArduinoPort("");
                    setContentLabelArduinoStatus("Desconectado");
                    setButtonConnectArduinoIsEnable(true);
                    MessageBox.Show("Al parecer el arduino que esta conectado en este puerto no es un arduino programado para realizar las funciones necesarias.", "Arduino incorrecto", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public bool openSerialPort(ImmutablePort port) {
            arduinoSerialPort = new SerialPort(port.Id, 9600);
            try {
                arduinoSerialPort.Open();
                //Console.WriteLine("Testing port...");
                /*arduinoSerialPort.Write("a?");
                Thread.Sleep(100);
                var res = arduinoSerialPort.ReadLine();
                if (res == "y") {
                    return true;
                } else {
                    arduinoSerialPort.Close();
                    return false;
                }*/
                return true;
            } catch (Exception e) {
                Console.WriteLine(": " + e.Message);
                MessageBox.Show("Al parecer este puerto ya esta siendo usado por otro proceso, Cierre la aplicacion que este usando este arduino para poder realizar la conexion", "Arduino incorrecto", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;

            }
        }

        public void setupReader() {
            SelectedReader = FingerPrint.findReader();
            if (SelectedReader != null) {
                addOnCaptureListener();
                var result = FingerPrint.initializeReader(SelectedReader);
                switch (result) {
                    case FingerPrint.Responses.SUCCESS:
                        setButtonInitializeReaderIsEnable(false);
                        setContentLabelReaderStatus("Conectado");
                        break;
                    case FingerPrint.Responses.CANT_OPEN_READER:
                        break;
                    case FingerPrint.Responses.ERROR:
                        break;
                }
            } else {
                MessageBox.Show("Al parecer no hay ningun lector conectado al computador, Conecte uno he intente inicializar de nuevo el lector", "Lector de huellas no conectado", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        protected void processFingerPrint(CaptureResult captureResult) {
            Console.WriteLine(captureResult.Quality.ToString());
            Console.WriteLine(captureResult.ResultCode.ToString());
            if (captureResult.Quality == Constants.CaptureQuality.DP_QUALITY_GOOD) {
                Console.WriteLine("Huella capturada con buena calidad");
                var fiv = captureResult.Data.Views[0];
                var bitmap = Helpers.CreateBitmap(fiv.RawImage, fiv.Width, fiv.Height);
                updateFingerPrintImage(bitmap);
                try {
                    DataResult<Fmd> conversionResult = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Constants.Formats.Fmd.ANSI);
                    if(SH.User != null && SH.User.bytesFingerprint != null) {
                        var comparisonResult = Comparison.Compare(conversionResult.Data, 0, SH.MainFingerPrint, 0);
                        if (comparisonResult.ResultCode == Constants.ResultCode.DP_SUCCESS && comparisonResult.Score < THRESH_HOLDER_SCORE) {
                            updateLabelStatus("Señor "+SH.User.FullName);
                            openDoorAndCloseAfter(DELAY_SECONDS);
                            publish("message", "open_door", SH.User .FullName);
                            var record = new Record();
                            record.personName = SH.User.FullName;
                            record.userId = SH.User._id;
                            record.date = getCurrentDateTime();
                            RestAPI.addRecord(SH.UserAccessToken, record, (IRestResponse response) => { });
                        } else {
                            IdentifyResult identifyResult = Comparison.Identify(conversionResult.Data, 0, SH.PeopleKnownFingerPrints, THRESH_HOLDER_SCORE, 1);
                            if (identifyResult.ResultCode == Constants.ResultCode.DP_SUCCESS) {
                                PersonKnown peopleKnowFound = null;
                                if (identifyResult.Indexes.Length > 0) {
                                    int index = identifyResult.Indexes[0][0];
                                    var idPeopleKnown = SH.PeopleKnownIds[index];
                                    peopleKnowFound = SH.User.peopleKnown.Find(pk => pk._id == idPeopleKnown);
                                    updateLabelStatus("Señor " + peopleKnowFound.FullName);
                                    openDoorAndCloseAfter(DELAY_SECONDS);
                                    publish("message", "open_door", peopleKnowFound.FullName);
                                    var record = new Record();
                                    record.personName = peopleKnowFound.FullName;
                                    record.userId = SH.User._id;
                                    record.date = getCurrentDateTime();
                                    RestAPI.addRecord(SH.UserAccessToken, record, (IRestResponse response) => { });
                                }
                                if (peopleKnowFound == null) {
                                    updateLabelStatus("Usuario desconocido");
                                    publish("message", "attempt_open_door", "onknown");
                                    var record = new Record();
                                    record.personName = "Desconocido";
                                    record.userId = SH.User._id;
                                    record.date = getCurrentDateTime();
                                    RestAPI.addRecord(SH.UserAccessToken, record, (IRestResponse response) => { });
                                }
                            } else {
                                updateLabelStatus("Usuario desconocido");
                                publish("message", "attempt_open_door", "onknown");
                                var record = new Record();
                                record.personName = "Desconocido";
                                record.userId = SH.User._id;
                                record.date = getCurrentDateTime();
                                RestAPI.addRecord(SH.UserAccessToken, record, (IRestResponse response) => { });
                            }
                        }
                    }
                } catch (Exception ex) {
                    Console.WriteLine("Error:  " + ex.Message);
                }
            }
        }

        private MyDateTime getCurrentDateTime() {
            var dt = DateTime.Now;
            var current = new MyDateTime();
            current.hour = dt.Hour;
            current.minute = dt.Minute;
            current.day = dt.Day;
            current.month = dt.Month;
            current.year = dt.Year;

            return current;
        }

        public void addOnCaptureListener() {
            if (SelectedReader != null) SelectedReader.On_Captured += readerOnCaptured;
        }

        public void removeOnCaptureListener() {
            if (SelectedReader != null) SelectedReader.On_Captured -= readerOnCaptured;
        }

        public bool openDoorAndCloseAfter(int seconds) {
            if(sendArduinoCommand("1")) {
                Timer timer = null;
                timer = new Timer((obj) => {
                    sendArduinoCommand("0");
                }, null, (seconds*1000), Timeout.Infinite);
                return true;
            }
            return false;
        }

        private bool sendArduinoCommand(string command) {
            if (arduinoSerialPort != null && arduinoSerialPort.IsOpen) {
                Console.WriteLine("Sending " + command + " to arduino");
                arduinoSerialPort.Write(command);
                return true;
            } else {
                Console.WriteLine("The arduinoSerialPort is null or closed");
            }
            return false;
        }

        public void updateLabelStatus(string status) {
            Dispatcher.InvokeAsync(() => {
                LabelStatusBar.Content = status;
            });
        }
        
        public void publish(string type, string rason, string username) {
            string topic = "/securityhouse/from/" + SH.User._id + "/house/" + SH.User.idHouse;
            Console.WriteLine(topic);
            var message = new MQTTMessageToSend<MQTTMessageToSendData>();
            message.replyTo = topic;
            message.type = type;
            var data = new MQTTMessageToSendData();
            data.houseId = SH.User.idHouse;
            data.rason = rason;
            data.person = username;
            message.data = data;
            mqttClient.Publish(topic, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)), MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE, false);
        }

        #endregion

        private void MenuItemRecords_Click(object sender, RoutedEventArgs e) {
            var w = new RecordsWindow();
            w.ShowDialog();
        }
    }
}
