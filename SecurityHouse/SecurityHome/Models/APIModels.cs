﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityHouse.Models {
    class Name {
        public string first { set; get; }
        public string last { set; get; }
    }

    class UserSchema {
        public int _id { get; set; }
        public Name name { get; set; }

        private string _fingerprint;
        public string fingerprint {
            get {
                return _fingerprint;
            }
            set {
                _fingerprint = value;
                try {
                    _bytesFingerprint = Convert.FromBase64String(value);
                }catch(Exception e) {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private byte[] _bytesFingerprint;
        public byte[] bytesFingerprint {
            get { return _bytesFingerprint; }
            set {
                _bytesFingerprint = value;
                try {
                    _fingerprint = Convert.ToBase64String(value);
                } catch (Exception e) {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public UserSchema() {
            name = new Name();
        }

        public string FullName {
            get { return name.first + " " + name.last; }
        }
    }

    class User : UserSchema {
        public string username { get; set; }
        public string idHouse { get; set; }
        public List<PersonKnown> peopleKnown { get; set; }
    }

    class PersonKnown : UserSchema {
        public string alias { get; set; }
    }

    class Response<T> {
        public string message { set; get; }
        public T data { get; set; }
    }

    class ErrorResponse {
        public string message { set; get; }
        public string error { set; get; }
        public int code { set; get; }
    }

    class MyDateTime {
        public int hour { set; get; }
        public int minute { set; get; }
        public int day { set; get; }
        public int month { set; get; }
        public int year { set; get; }

        public MyDateTime() : this(0,0,0,0,0){ }

        public MyDateTime(int hour, int minute, int day, int month, int year) {
            this.minute = minute;
            this.hour = hour;
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public MyDateTime(DateTime dateTime) {
            if (dateTime != null) {
                hour = dateTime.Hour;
                minute = dateTime.Minute;
                day = dateTime.Day;
                month = dateTime.Month;
                year = dateTime.Year;
            } else {
                hour = 0;
                minute = 0;
                day = 0;
                month = 0;
                year = 0;
            }
        }

        public new string ToString {
            get {
                return hour + ":" + minute + " " + day + "/" + month + "/" + year;
            }
        }
    }

    class Record {
        public int _id { set; get; }
        public int userId { set; get; }
        public MyDateTime date { set; get; }
        public string personName { set; get; }
    }

    class MQTTMessageReceived {
        public string replyTo { set; get; }
        public string type { set; get; }
        public string command { set; get; }
    }

    class MQTTMessageToSend<T> {
        public string replyTo { set; get; }
        public string type { set; get; }
        public T data { set; get; }
    }

    public class MQTTResponseToSendData {
        public string houseId { set; get; }
        public string message { set; get; }
    }

    public class MQTTMessageToSendData {
        public string houseId { set; get; }
        public string rason { set; get; }
        public string person { set; get; }
    }
}
