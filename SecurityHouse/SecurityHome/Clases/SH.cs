﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SecurityHouse.Models;
using DPUruNet;

namespace SecurityHouse {
    class SH {
        public static List<Fmd> PeopleKnownFingerPrints { get; set; }
        public static List<int> PeopleKnownIds { get; set; }
        public static Fmd MainFingerPrint { get; set; }
        public static List<int> Ids { get; set; }
        public static User User { get; set; }
        public static string UserAccessToken {set; get;}
        public static int UserId { set; get;}
    }
}
