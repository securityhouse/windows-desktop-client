﻿using DPUruNet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SecurityHouse.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace SecurityHouse {
    static class Helpers {
        public static BitmapImage CargarImagenDesdeBytes(byte[] imageData) {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData)) {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        public static BitmapImage BitmapToImageSource(Bitmap bitmap) {
            using (MemoryStream memory = new MemoryStream()) {
                bitmap.Save(memory, ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public static byte[] getBytesFromBitmapImage(BitmapImage imageC) {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.QualityLevel = 30;
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.GetBuffer();
        }

        public static Bitmap CreateBitmap(Byte[] bytes, int width, int height) {
            byte[] rgbBytes = new byte[bytes.Length * 3];
            for (int i = 0; i <= bytes.Length - 1; i++) {
                rgbBytes[(i * 3)] = bytes[i];
                rgbBytes[(i * 3) + 1] = bytes[i];
                rgbBytes[(i * 3) + 2] = bytes[i];
            }
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            BitmapData data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            for (int i = 0; i <= bmp.Height - 1; i++) {
                IntPtr p;
                if (IntPtr.Size == 8) {
                    p = new IntPtr(data.Scan0.ToInt64() + data.Stride * i);
                } else {
                    p = new IntPtr(data.Scan0.ToInt32() + data.Stride * i);
                }
                System.Runtime.InteropServices.Marshal.Copy(rgbBytes, i * bmp.Width * 3, p, bmp.Width * 3);
            }
            bmp.UnlockBits(data);
            return bmp;
        }

        public static void proccessGetUserContent(string content, Action done) {
            if (content != "") {
                JObject json = JObject.Parse(content);
                SH.User = JsonConvert.DeserializeObject<User>(json["data"].ToString());
                if (SH.User.bytesFingerprint != null) {
                    SH.MainFingerPrint = new Fmd(SH.User.bytesFingerprint, (int)Constants.Formats.Fmd.ANSI, Constants.WRAPPER_VERSION);
                }
                SH.PeopleKnownFingerPrints = new List<Fmd>();
                SH.PeopleKnownIds = new List<int>();
                if (SH.User.peopleKnown != null) {
                    foreach (var pk in SH.User.peopleKnown) {
                        if (pk.bytesFingerprint != null) {
                            SH.PeopleKnownFingerPrints.Add(new Fmd(pk.bytesFingerprint, (int)Constants.Formats.Fmd.ANSI, Constants.WRAPPER_VERSION));
                            SH.PeopleKnownIds.Add(pk._id);
                        }
                    }
                }
            }
            done();
        }
    }
}
