﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RestSharp;
using SecurityHouse.Models;

namespace SecurityHouse {
    class RestAPI {
        public static readonly string API_URL = "http://securityhouse.ssg.company";
        //public static readonly string API_URL = "http://192.168.1.11:3000";

        public static void signUp(string username, string password, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("auth/signup", Method.POST);
            request.AddParameter("username", username);
            request.AddParameter("password", password);

            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }

        public static void login(string username, string password, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("auth/login", Method.POST);
            request.AddParameter("username", username);
            request.AddParameter("password", password);

            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }

        public static void getUser(string token, int id, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("users/"+id.ToString(), Method.GET);
            request.AddHeader("Authorization", "Bearer " + token);
            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }

        public static void updateProfile(string token, User user, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("users/"+user._id, Method.PUT);
            request.AddHeader("Authorization", "Bearer " + token);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(user);
            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }

        public static void addPersonKnown(string token, int userId, PersonKnown person, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("users/" + userId + "/peopleKnown", Method.PUT);
            request.AddHeader("Authorization", "Bearer " + token);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(person);
            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }

        public static void deletePersonKnown(string token, int userId, int personId, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("users/" + userId + "/peopleKnown/" + personId, Method.DELETE);
            request.AddHeader("Authorization", "Bearer " + token);
            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }

        public static void addRecord(string token, Record record, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("records", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(record);
            request.AddHeader("Authorization", "Bearer " + token);
            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }

        public static void getRecord(string token, Action<IRestResponse> callback) {
            var client = new RestClient();
            client.BaseUrl = new Uri(API_URL);
            var request = new RestRequest("records", Method.GET);
            request.AddHeader("Authorization", "Bearer " + token);
            client.ExecuteAsync(request, (IRestResponse response) => {
                callback(response);
            });
        }
    }
}
