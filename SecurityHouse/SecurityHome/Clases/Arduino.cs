﻿using System;
using System.Collections.Generic;
using System.Management;

namespace SecurityHouse {
    public sealed class ArduinoPortsManager {
        public static readonly ArduinoPortsManager Instance = new ArduinoPortsManager();
        public List<ImmutablePort> Ports = new List<ImmutablePort>();
        public event EventHandler OnPortsChanged;

        private ArduinoPortsManager() {
            try {
                using (ManagementEventWatcher connectedSerialPortwatcher = new ManagementEventWatcher(new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_SerialPort'")), disconnectedSerialPortwatcher = new ManagementEventWatcher(new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_SerialPort'"))) {
                    connectedSerialPortwatcher.EventArrived += onSerialPortConnected;
                    connectedSerialPortwatcher.Start();
                    disconnectedSerialPortwatcher.EventArrived += onSerialPortDisconnected;
                    disconnectedSerialPortwatcher.Start();
                }
                discoverArduinoPorts();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        private void onSerialPortDisconnected(object sender, EventArrivedEventArgs e) {
            discoverArduinoPorts();
        }

        private void onSerialPortConnected(object sender, EventArrivedEventArgs e) {
            discoverArduinoPorts();
        }

        private void discoverArduinoPorts() {
            Ports = getArduinoPorts();
            if (OnPortsChanged != null) {
                OnPortsChanged(this, new EventArgs());
            }
        }

        private List<ImmutablePort> getArduinoPorts() {
            Console.WriteLine("Discovering Arduino Ports");
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(new ManagementScope(), new SelectQuery("SELECT * FROM Win32_SerialPort WHERE Description LIKE '%Arduino%'"))) {
                ManagementObjectCollection items = searcher.Get();
                var list = new List<ImmutablePort>();
                foreach (var item in items) {
                    String portDescription = item["Description"].ToString();
                    String portID = item["DeviceID"].ToString();
                    if (portDescription.Contains("Arduino")) {
                        list.Add(ImmutablePort.CreateBuilder(portID).withDescription(portDescription).build());
                    }
                }
                return list;   
            }
        }
    }

    public sealed class ImmutablePort {
        private readonly string _id;
        private readonly string _description;
        
        private ImmutablePort(string id, string description) {
            _id = id;
            _description = description;
        }

        public string Id { get { return _id; } }
        public string Description { get { return _description; } }
        public string DisplayData { get { return _id + " / " + _description; } }

        private interface IPortBuilder {
            Builder Create(string id);
            Builder withDescription(string description);
            ImmutablePort build();
        }

        public sealed class Builder : IPortBuilder {
            private string Id;
            private string Description;

            public Builder Create(string id) {
                Id = id;
                return this;
            }

            public Builder withDescription(string description) {
                Description = description;
                return this;
            }

            public ImmutablePort build() {
                return new ImmutablePort(this.Id, this.Description);
            }   
        }

        public static Builder CreateBuilder(string id) {
            return new Builder().Create(id);
        }
    }
}
