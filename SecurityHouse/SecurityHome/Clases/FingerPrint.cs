﻿using System;
using DPUruNet;
using System.Threading;

namespace SecurityHouse {
    public static class FingerPrint {
        public static Reader findReader() {
            ReaderCollection rc = ReaderCollection.GetReaders();
            if (rc.Count > 0) {
                return rc[0];
            } else {
                return null;
            }
        }

        public static Responses initializeReader(Reader reader) {
            if(reader == null) {
                return Responses.NULL_READER;
            }
            Console.WriteLine("Trying to opent the reader " + reader.Description.SerialNumber);
            try {
                Constants.ResultCode result = reader.Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE);
                if (result != Constants.ResultCode.DP_SUCCESS) {
                    Console.WriteLine("Error intentando abrir el lector");
                    Console.WriteLine("Error: " + result.ToString());
                    return Responses.CANT_OPEN_READER;
                }    
                Console.WriteLine("Reader opened");
                reader.GetStatus();
                if (reader.Status != null) {
                    Console.WriteLine("Status.Status: " + reader.Status.Status);
                    if (reader.Status.Status == Constants.ReaderStatuses.DP_STATUS_BUSY) {
                        Console.WriteLine("The reader is busy");
                        Thread.Sleep(50);
                    } else if (reader.Status.Status == Constants.ReaderStatuses.DP_STATUS_NEED_CALIBRATION) {
                        Console.WriteLine("The reader needs calibration");
                        reader.Calibrate();
                    }
                } else {
                    Console.WriteLine("Status devolvio un valor nulo");
                }
                Constants.ResultCode resultCaptureAsync = reader.CaptureAsync(Constants.Formats.Fid.ANSI, Constants.CaptureProcessing.DP_IMG_PROC_DEFAULT, reader.Capabilities.Resolutions[0]);
                if (resultCaptureAsync == Constants.ResultCode.DP_SUCCESS) {
                    Console.WriteLine("Captura asincrona iniciada");
                    return Responses.SUCCESS;
                } else {
                    Console.WriteLine("Error iniciando la captura asincrona: " + resultCaptureAsync.ToString());
                    return Responses.ERROR;
                }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
                return Responses.ERROR;
            }
        }

        public static Responses closeReader(Reader reader) {
            if (reader == null) return Responses.NULL_READER;
            reader.CancelCapture();
            reader.Dispose();
            //reader.Dispose();
            return Responses.SUCCESS;
        }

        public enum Responses {
            NULL_READER,
            CANT_OPEN_READER,
            SUCCESS,
            ERROR
        }
    }
}
